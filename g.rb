#!/usr/bin/env ruby
# ruby g.rb dogma job_titles ./build

require 'mustache'
require 'nokogiri'
require 'active_support/inflector'

# Arguments
namespace = ARGV[0]
app_name = ARGV[1]
dest_dir = ARGV[2]
source_dir = "./template"

def reset_name(value)
  value.underscore.singularize
end

def do_folder(source_dir, dest_dir)
  Dir.foreach source_dir do |entry|
    next if entry == '.' or entry == '..'

    source_path = "#{source_dir}/#{entry}"
    if Dir.exists? source_path
      new_dir = "#{dest_dir}/#{parse_content(entry)}"

      Dir::mkdir new_dir unless FileTest::directory? new_dir

      do_folder source_path, new_dir
    else
      do_file(entry, source_path, dest_dir)
    end
  end
end

def do_file(file_name, file_path, dest_dir)
  template = File.read file_path

  new_name = parse_content(file_name)

  puts new_name
  File.open "#{dest_dir}/#{new_name}", "w" do |file|
    file.puts parse_content(template)
  end
end

def parse_content(content)
  Mustache.render(content, @moustache_context)
end

@namespace = reset_name(namespace)
@app_name = reset_name(app_name)

@moustache_context = {
  :namespace => @namespace,
  :app_name => @app_name,
  :namespace_titleized => @namespace.titleize,
  :app_name_titleized => @app_name.titleize
}

@moustache_context[:namespace_file] = @moustache_context[:namespace_titleized].gsub(/ /, '_')
@moustache_context[:app_name_file] = @moustache_context[:app_name_titleized].gsub(/ /, '_')
@moustache_context[:app_name_file_pluralized] = @moustache_context[:app_name_file].pluralize

@moustache_context[:file_name] = "#{@moustache_context[:namespace_file]}_#{@moustache_context[:app_name_file]}"
@moustache_context[:file_name_pluralized] = "#{@moustache_context[:namespace_file]}_#{@moustache_context[:app_name_file_pluralized]}"

@moustache_context[:app_name_downcase] = @moustache_context[:app_name_titleized].downcase
@moustache_context[:app_name_pluralized] = @moustache_context[:app_name].pluralize
@moustache_context[:app_name_downcase_pluralized] = @moustache_context[:app_name_downcase].pluralize

Dir::mkdir dest_dir unless FileTest::directory? dest_dir

app_dir = "#{dest_dir}/#{@app_name}"
Dir::mkdir app_dir unless FileTest::directory? app_dir

do_folder(source_dir, app_dir)