<?php
    /*
        This file includes the code called by the site pages at runtime.
        If you app is admin-only then don't include this file.
        
        Remember - try and be as lightweight at runtime as possble. Include only 
        what you need to, run only 100% necessary code. Make every database query
        count.
    */

    # Include your class files as needed - up to you.
    include('{{file_name_pluralized}}.class.php');
    include('{{file_name}}.class.php');


    # Create the function(s) users will call from their pages
    
    
    /**
     * 
     * Builds an listing of {{app_name_downcase_pluralized}}. Echoes out the resulting mark-up and content
     * @param string $template
     * @param bool $return if set to true returns the output rather than echoing it
     */
    function {{app_name}}_listing($template='listing.html', $return=false)
    {
        $API  = new PerchAPI(1.0, '{{app_name}}');
        ${{app_name_file_pluralized}} = new {{file_name_pluralized}}($API);
    
        $list = ${{app_name_file_pluralized}}->all();
        
        $Template = $API->get('Template');
        $Template->set('{{app_name}}/'.$template, '{{app_name}}');

        $r = $Template->render_group($list, true);
        
        if ($return) return $r;
        echo $r;
        
        return false;
    }
    
    /**
     * 
     * Renders the detail page of a {{app_name_downcase}} based on {{app_name}}ID
     * @param int ${{app_name}}ID
     * @param string $template
     * @param bool $return if set to true returns the output rather than echoing it
     */
    function {{app_name}}_detail(${{app_name}}ID, $template='detail.html', $return=false)
    {
    	$API  = new PerchAPI(1.0, '{{app_name}}');
        ${{app_name_file_pluralized}} = new {{file_name_pluralized}}($API);
        
        ${{app_name_file}} = ${{app_name_file_pluralized}}->find(${{app_name}}ID, true);
        
        $list = array(${{app_name_file}});
        
        $Template = $API->get('Template');
        $Template->set('{{app_name}}/'.$template, '{{app_name}}');

        $r = $Template->render_group($list, true);
        
        if ($return) return $r;
        echo $r;
        
        return false;
    
    }
    

?>