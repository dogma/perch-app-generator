<?php
    // Prevent running directly:
    if (!defined('PERCH_DB_PREFIX')) exit;

    // Let's go
    $sql = "
    CREATE TABLE IF NOT EXISTS `__PREFIX__{{namespace}}_{{app_name_pluralized}}` (
      `{{app_name}}ID` int(11) NOT NULL AUTO_INCREMENT,
      `{{app_name}}Title` varchar(255) NOT NULL DEFAULT '',
      `{{app_name}}DateTime` datetime DEFAULT NULL,
      `{{app_name}}DescRaw` text DEFAULT NULL,
      `{{app_name}}DescHTML` text DEFAULT NULL,
      PRIMARY KEY (`{{app_name}}ID`)
    ) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;";
    
    $sql = str_replace('__PREFIX__', PERCH_DB_PREFIX, $sql);
    
    $statements = explode(';', $sql);
    foreach($statements as $statement) {
        $statement = trim($statement);
        if ($statement!='') $this->db->execute($statement);
    }
        
    $sql = 'SHOW TABLES LIKE "'.$this->table.'"';
    $result = $this->db->get_value($sql);
    
    return $result;
?>