<?php

class {{file_name_pluralized}} extends PerchAPI_Factory
{
    protected $table     = '{{namespace}}_{{app_name_pluralized}}';
	protected $pk        = '{{app_name}}ID';
	protected $singular_classname = '{{file_name}}';
	
	protected $default_sort_column = '{{app_name}}DateTime';
	
	
	
    function __construct($api=false) 
    {
        $this->cache = array();
        parent::__construct($api);
    }
}
    
?>