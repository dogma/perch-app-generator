<?php

class {{file_name}} extends PerchAPI_Base
{
    protected $table     = '{{namespace}}_{{app_name_pluralized}}';
	protected $pk        = '{{app_name}}ID';
}

?>