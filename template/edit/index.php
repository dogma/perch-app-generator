<?php
    # include the API
    include('../../../../core/inc/api.php');
    
    # include your class files
    include('../{{file_name_pluralized}}.class.php');
    include('../{{file_name}}.class.php');
    
    $API  = new PerchAPI(1.0, '{{app_name}}');
    
    # Grab an instance of the Lang class for translations
    $Lang = $API->get('Lang');

    # Set the page title
    $Perch->page_title = $Lang->get('Edit') . ' - ' .$Lang->get('{{app_name_titleized}}');

    # Do anything you want to do before output is started
    include('../modes/edit.pre.php');
    
    
    # Top layout
    include(PERCH_CORE . '/inc/top.php');

    
    # Display your page
    include('../modes/edit.post.php');
    
    
    # Bottom layout
    include(PERCH_CORE . '/inc/btm.php');
?>