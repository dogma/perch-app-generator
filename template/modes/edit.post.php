<?php

    # Side panel
    echo $HTML->side_panel_start();
    echo $HTML->heading3($heading2);
    echo $HTML->side_panel_end();
    
    
    # Main panel
    echo $HTML->main_panel_start();
    if ($message) echo $message; 
    echo $HTML->heading2('{{app_name_titleized}} details');
    
    echo $Form->form_start();
    
        echo $Form->text_field('{{app_name}}Title', 'Title', isset($details['{{app_name}}Title'])?$details['{{app_name}}Title']:false);
        
        echo $Form->textarea_field('{{app_name}}DescRaw', 'Post', isset($details['{{app_name}}DescRaw'])?$details['{{app_name}}DescRaw']:false);
        
        echo $Form->date_field('{{app_name}}DateTime', 'Date', isset($details['{{app_name}}DateTime'])?$details['{{app_name}}DateTime']:false, true);
        
        echo $Form->submit_field('btnSubmit', 'Save', $API->app_path());

    
    echo $Form->form_end();
    
    
    
    echo $HTML->main_panel_end();


?>