<?php
    
    ${{app_name_file_pluralized}} = new {{file_name_pluralized}}($API);

    $HTML = $API->get('HTML');
    $Form = $API->get('Form');
	$Form->require_field('{{app_name}}ID', 'Required');
	
	$message = false;
	
	//check that we have the ID of a {{app_name}}, if not redirect back to the listing.
	if (isset($_GET['id']) && $_GET['id']!='') {
	    ${{app_name_file}} = ${{app_name_file_pluralized}}->find($_GET['id'], true);
	}else{
	    PerchUtil::redirect($API->app_path());
	}
	
	// if the confirmation of delete has been submitted this is a form post
    if ($Form->submitted()) {
    	$postvars = array('{{app_name}}ID');
		
    	$data = $Form->receive($postvars);
    	
    	${{app_name_file}} = ${{app_name_file_pluralized}}->find($data['{{app_name}}ID'], true);
    	
    	if (is_object(${{app_name_file}})) {
    	    ${{app_name_file}}->delete();
            PerchUtil::redirect($API->app_path().'/');
        }else{
            $message = $HTML->failure_message('Sorry, that {{app_name_downcase}} could not be deleted.');
        }
    }

    
    
    $details = ${{app_name_file}}->to_array();



?>
