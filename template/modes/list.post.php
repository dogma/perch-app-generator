<?php

    # Side panel
    echo $HTML->side_panel_start();
    echo $HTML->heading3('New {{app_name_titleized}}');
    
    
    echo $HTML->para('%sAdd new {{app_name_downcase}}%s', '<a href="'.$HTML->encode(PERCH_LOGINPATH.'/addons/apps/{{app_name}}/edit/').'">', '</a>');
    
    echo $HTML->side_panel_end();
    
    
    # Main panel
    echo $HTML->main_panel_start();
    
    if (isset($message)) echo $message;
    
    if (PerchUtil::count($posts)) {
?>
    <table class="d">
        <thead>
            <tr>
                <th class="first"><?php echo $Lang->get('Title'); ?></th>  
                <th><?php echo $Lang->get('Date'); ?></th>
                <th class="action last"></th>
            </tr>
        </thead>
        <tbody>
<?php
    foreach($posts as $Post) {
?>
            <tr>
                <td><a href="<?php echo $HTML->encode(PERCH_LOGINPATH); ?>/addons/apps/{{app_name}}/edit/?id=<?php echo $HTML->encode(urlencode($Post->id())); ?>" class="edit"><?php echo $HTML->encode($Post->{{app_name}}Title()); ?></a></td>
                
                <td><?php echo $HTML->encode(strftime('%d %B %Y, %l:%M %p', strtotime($Post->{{app_name}}DateTime()))); ?></td>
                <td><a href="<?php echo $HTML->encode(PERCH_LOGINPATH); ?>/addons/apps/{{app_name}}/delete/?id=<?php echo $HTML->encode(urlencode($Post->id())); ?>" class="delete"><?php echo $Lang->get('Delete'); ?></a></td>
            </tr>

<?php   
    }
?>
        </tbody>
    </table>
<?php    
       

    } // if pages
    
    echo $HTML->main_panel_end();


?>