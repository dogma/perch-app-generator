<?php
    
    # Side panel
    echo $HTML->side_panel_start();
    echo $HTML->heading3('Delete {{app_name_titleized}}');
    echo $HTML->para('Delete a {{app_name_downcase}} here.');
    echo $HTML->side_panel_end();
    
    
    # Main panel
    echo $HTML->main_panel_start(); 
    echo $Form->form_start();
    
    if ($message) {
        echo $message;
    }else{
        echo $HTML->warning_message('Are you sure you wish to delete the {{app_name_downcase}} %s?', $details['{{app_name}}Title']);
        echo $Form->form_start();
        echo $Form->hidden('{{app_name}}ID', $details['{{app_name}}ID']);
		echo $Form->submit_field('btnSubmit', 'Delete', $API->app_path());


        echo $Form->form_end();
    }
    
    echo $HTML->main_panel_end();

?>
