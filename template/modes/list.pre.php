<?php
    
    $HTML = $API->get('HTML');
    
    ${{app_name_file_pluralized}} = new {{file_name_pluralized}}($API);
   
    $posts = array();
	
    $posts = ${{app_name_file_pluralized}}->all();
            
    // Install only if $posts is false. 
    // This will run the code in activate.php so should only ever happen on first run - silently installing the app.
    if ($posts == false) {
    	${{app_name_file_pluralized}}->attempt_install();
    }
?>