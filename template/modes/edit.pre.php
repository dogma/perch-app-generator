<?php

	${{app_name_file_pluralized}} = new {{file_name_pluralized}}($API);

    $HTML = $API->get('HTML');
    $Form = $API->get('Form');
    $Text = $API->get('Text');
    
    $result = false;
    $message = false;
    
    //check to see if we have an ID on the QueryString
    if (isset($_GET['id']) && $_GET['id']!='') {
    	//If yes, this is an edit. Get the object and turn it into an array
        ${{app_name}}ID = (int) $_GET['id'];    
        ${{app_name_file}} = ${{app_name_file_pluralized}}->find(${{app_name}}ID, true);
        $details = ${{app_name_file}}->to_array();
        
        $heading1 = '{{app_name_titleized}} / Edit {{app_name_titleized}}';
        $heading2 = 'Edit {{app_name_titleized}}';
        
    }else{
    	//If no, we're adding a new one
        ${{app_name_file}} = false;
        ${{app_name}}ID = false;
        $details = array();
        
        $heading1 = '{{app_name_titleized}} / Create New {{app_name_titleized}}';
        $heading2 = 'Add {{app_name_titleized}}';
    }
    
    //set required fields
    $Form->require_field('{{app_name}}Title', 'Required');
    
    
    if ($Form->submitted()) {
        $postvars = array('{{app_name}}ID', '{{app_name}}Title', '{{app_name}}DescRaw');
    	$data = $Form->receive($postvars);
        
    	//picks up the date field and assembles a date that MySQL will be happy with
    	$data['{{app_name}}DateTime'] = $Form->get_date('{{app_name}}DateTime');
    	
    	//transform the raw field into html
    	$data['{{app_name}}DescHTML'] = $Text->text_to_html($data['{{app_name}}DescRaw']);
    	
    	//if we have a {{app_name_file}} update it
    	if (is_object(${{app_name_file}})) {
    	    $result = ${{app_name_file}}->update($data);
    	}else{
    		//otherwise create it
    	    if (isset($data['{{app_name}}ID'])) unset($data['{{app_name}}ID']);
    	    
    	    $new_{{app_name}} = ${{app_name_file_pluralized}}->create($data);
    	    
    	    if ($new_{{app_name}}) {
    	        PerchUtil::redirect($API->app_path() .'/edit/?id='.$new_{{app_name}}->id().'&created=1');
    	    }else{
    	        $message = $HTML->failure_message('Sorry, that {{app_name_downcase}} could not be updated.');
    	    }
    	}
    	
    	
        if ($result) {
            $message = $HTML->success_message('Your {{app_name_downcase}} has been successfully updated. Return to %s{{app_name_downcase}} listing%s', '<a href="'.$API->app_path() .'">', '</a>');  
        }else{
            $message = $HTML->failure_message('Sorry, that {{app_name_downcase}} could not be updated.');
        }
        
        if (is_object(${{app_name_file}})) {
            $details = ${{app_name_file}}->to_array();
        }else{
            $details = array();
        }
        
    }
    
    if (isset($_GET['created']) && !$message) {
        $message = $HTML->success_message('Your {{app_name_downcase}} has been successfully created. Return to %s{{app_name_downcase}} listing%s', '<a href="'.$API->app_path() .'">', '</a>'); 
    }
    
?>